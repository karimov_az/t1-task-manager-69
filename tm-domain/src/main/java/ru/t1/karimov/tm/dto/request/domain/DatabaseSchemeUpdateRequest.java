package ru.t1.karimov.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DatabaseSchemeUpdateRequest extends AbstractUserRequest {

    public DatabaseSchemeUpdateRequest(@Nullable final String token) {
        super(token);
    }

}
