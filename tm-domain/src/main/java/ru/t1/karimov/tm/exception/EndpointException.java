package ru.t1.karimov.tm.exception;

import org.jetbrains.annotations.NotNull;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull final String message) {
        super(message);
    }

}
