package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    @Override
    void removeOne(@Nullable User model);

    @Override
    void removeAll();

    void removeOneByLogin(@Nullable String login);

    void removeOneByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void unlockUserByLogin(@Nullable String login);

}
