package ru.t1.karimov.tm.service.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.model.Session;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

}
