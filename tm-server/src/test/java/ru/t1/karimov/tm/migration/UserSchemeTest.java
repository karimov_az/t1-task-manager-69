package ru.t1.karimov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("user");
    }

}
