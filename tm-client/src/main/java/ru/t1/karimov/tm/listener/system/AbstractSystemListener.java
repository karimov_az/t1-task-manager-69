package ru.t1.karimov.tm.listener.system;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.listener.AbstractListener;

import java.util.List;

@Component
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected List<AbstractListener> listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
