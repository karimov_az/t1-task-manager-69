package ru.t1.karimov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.request.task.TaskListRequest;
import ru.t1.karimov.tm.dto.response.task.TaskListResponse;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Show list tasks.";

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[TASKS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), sort);
        @NotNull final TaskListResponse response = taskEndpoint.listTask(request);

        @NotNull final List<TaskDto> tasks = response.getTaskList();
        renderTasks(tasks);
    }

}
