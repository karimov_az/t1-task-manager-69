package ru.t1.karimov.tm.listener.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.karimov.tm.listener.AbstractListener;

@Component
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
