package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("api/tasks")
public interface ITaskRestEndpoint {

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @PostMapping("delete/{id}")
    @WebMethod(operationName = "deleteById")
    void delete(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @Nullable
    @WebMethod
    @GetMapping("findById/{id}")
    Task findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

}
