package ru.t1.karimov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.karimov.tm.model.CustomUser;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;

import java.util.Collection;

@Controller
public class ProjectsController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private Collection<Project> getProjects(@NotNull final CustomUser user) {
        return projectService.findAllByUserId(user.getUserId());
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", getProjects(user));
    }

}
