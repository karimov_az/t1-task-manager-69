package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(String login);

    boolean existsByLogin(String login);

}
