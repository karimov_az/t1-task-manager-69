package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.karimov.tm.dto.soap.*;
import ru.t1.karimov.tm.service.TaskService;
import ru.t1.karimov.tm.util.UserUtil;

@Endpoint
public class TaskSoapEndpointImpl {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.karimov.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    private TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.removeByUserId(request.getTask(), UserUtil.getUserId());
        return new TaskDeleteResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.removeByIdAndUserId(request.getId(), UserUtil.getUserId());
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    private TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAllByUserId(UserUtil.getUserId()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    private TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        response.setTask(taskService.findOneByIdAndUserId(request.getId(), UserUtil.getUserId()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    private TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.addByUserId(request.getTask(), UserUtil.getUserId());
        @NotNull final TaskSaveResponse response = new TaskSaveResponse();
        response.setTask(request.getTask());
        return response;
    }

}
