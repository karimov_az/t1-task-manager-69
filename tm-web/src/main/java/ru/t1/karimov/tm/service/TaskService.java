package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public void add(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final Task model, @Nullable final String userId) {
        if (model == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    public Task findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public Task findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findByIdAndUserId(id, userId);
    }

    @Transactional
    public void remove(@Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeByUserId(@Nullable final Task model, @Nullable final String userId) {
        if (model == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByIdAndUserId(model.getId(), userId);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteById(id);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Transactional
    public void update(@Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.save(model);
    }

}
