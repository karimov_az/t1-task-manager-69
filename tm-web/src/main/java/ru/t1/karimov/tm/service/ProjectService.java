package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Project;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public void add(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final Project model, @Nullable final String userId) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    public Project findOneById(@Nullable final String id) {
        if (id == null) throw new IdEmptyException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    public Project findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void remove(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.deleteById(id);
    }

    @Transactional
    public void removeByUserId(@Nullable final Project model, @Nullable final String userId) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void update(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @NotNull
    public String getProjectName(@Nullable final String id) {
        @Nullable final Project project = findOneById(id);
        if (project == null) return "";
        @NotNull final String name = project.getName();
        return name;
    }

}
